import java.util.Random;

public class Board{
	
	private Tile[][] grid;
	private final int SIZE = 5;
	
	public Board(){
		this.grid = new Tile[SIZE][SIZE];
		
		Random rng = new Random();
		
		for(int i = 0; i < this.grid.length ; i++){
			for(int x = 0; x < this.grid[i].length ; x++){
				this.grid[i][x] = Tile.BLANK;
			}
		}
		for(int i = 0; i < this.grid.length ; i++){
			int randomX = rng.nextInt(this.grid[i].length);
			this.grid[i][randomX] = Tile.HIDDEN_WALL;
		}
	}
		
	public String toString(){
		String board = "";
		for(Tile[] row : this.grid){
			board += "\n";
			for(Tile i : row){
				board += i.toString();
			}
		}
		return board;
	}
	
	public int placeToken(int row, int col) {
		if (row < 0 || row >= SIZE || col < 0 || col >= SIZE) {
            return -2;
        }
        if (grid[row][col] == Tile.CASTLE || grid[row][col] == Tile.WALL) {
            return -1;
        } 
		if(grid[row][col] == Tile.HIDDEN_WALL){
			grid[row][col] = Tile.WALL;
			return 1;
		} else {
			grid[row][col] = Tile.CASTLE;
			return 0;
		}
	}
}