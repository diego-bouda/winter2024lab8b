import java.util.Scanner;

public class BoardGameApp{
	public static void main(String[] args) {
		Board board = new Board();
		
		System.out.println("Welcome to the a simple board game!");
		
		int numCastles = 5; 
		int turns = 0;
		
		Scanner scanner = new Scanner(System.in);
		
		while(numCastles > 0 && turns < 8){
			
			System.out.println(board.toString());
			
			System.out.println("Number of castles: " + numCastles);
			System.out.println("Number of turns: " + turns);
			
			System.out.println("Choose a row number between 0-4");
			int row = scanner.nextInt();
			System.out.println("Choose a column number between 0-4");
			int col = scanner.nextInt();
			
			int response = board.placeToken(row, col);
			
			if (response < 0) {
				System.out.println("Invalid move. Try again.");
			} 
			if(response == 1){
				System.out.println("There was a wall at that position!");
				turns -= 1;
			}
			if(response == 0){
				System.out.println("You placed a castle!");
				turns += 1;
				numCastles -= 1;
			}
		}
	
		System.out.println("This is the final board :" + board.toString());
		
		if(numCastles == 0){
			System.out.println("You won!");
		}else{
			System.out.println("You lost!");
		}
	}
}